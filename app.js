let PlanCompoent= {
  template: '#plan-template',
props:{
  name:{
    type:String,
    required:true
  },
  selectedPlan:{
    type:String
  }
},
methods:{
  select() {
    this.$emit('select',this.name)
  }
},
computed:{
  isSelected(){
   // console.log("called " + this.selectedPlan)
    return this.name == this.selectedPlan
  }
}
// data: function () {
// return {
//   count: 0
// }
// },

}


let todoItemComponent = {
 template: "#todo-item-template"
}


let PlanPickerComponet = {
   template: '#plan-picker-template',
data: function () {
return {
  plans:["rose","mango","banana","apple","riyal","riad","shahriar","tawkir"],
  selectedPlan : null
}
},
methods:{
  selectPlan(plan) {
    console.log(plan)
    this.selectedPlan=plan
  }
},
components: {
  plan:PlanCompoent
},

}






var app = new Vue({
  el: '#app',
  components:{
    "plan-picker":PlanPickerComponet,
    "todo-item" : todoItemComponent
  }
  
})
